function getHand() {
  let hand = []
  let n = 0
  while (n <= 7) {
    let alphabet = [..."abcdefghijklmnopqrstuvwxyz"]
    let index = Math.floor(Math.random() * 27)
    hand.push(alphabet[index])
    n++
  }
  return hand
}

console.log(getHand())