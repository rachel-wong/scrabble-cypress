/// <reference types="Cypress" />

// For form submission resulting in successful scoring, run scoring.spec.js

describe("Input Form", () => {
  beforeEach(() => {
    cy.startNewGame()
  })

  context("Case sensitivity", () => {
    it("Accepts lowercase", () => {
      const expectedValue = "hello"
      cy.get('[data-cy="wordInputForm"]').type(expectedValue)
        .should("have.value", expectedValue)
    })
    it("Accepts UPPERCASE", () => {
      const expectedValue = "HELLO"
      cy.get('[data-cy="wordInputForm"]').type(expectedValue)
        .should("have.value", expectedValue)
    })
  })

  context("Form Submission with NO HAND", () => {
    it("No score for empty SPACE input", () => {
      const emptyInput = " "
      cy.submitWord(emptyInput)
      cy.displayWarning("You must generate a hand first.")
    })
  })

  context("Form Submission WITH HAND", () => {
    it("No score for EMPTY SPACE input", () => {
      cy.get('[data-cy="newHandBtn"]').should("exist").click()
      const emptyInput = " "
      cy.submitWord(emptyInput)
      cy.get('[data-cy="handDisplay"]')
        .invoke('text')
        .then((hand) => {
          hand = hand.split(",")
          expect(hand).to.have.lengthOf(7)
        })
      cy.wordNotAccepted()
      cy.displayWarning("You must only use the number of letters in your hand.")
      cy.formClears()
    })

    it("No score for non-alphabetical input", () => {
      const expectedValue = "hop123!"
      cy.get('[data-cy="newHandBtn"]').should("exist").click()
      cy.submitWord(expectedValue)
      cy.displayWarning("Alphabets only, no numbers or symbols.")
    })

    it("No score for submitting same word more than once IN ONE HAND", () => {
      cy.seedCurrentHistory()
      const sampleHand = "biblica"
      cy.seedNewHand(sampleHand)
      const sampleWord = "bi"
      const sampleScore = 8

      // action - 2x submission
      let runTimes = 2
      while (runTimes > 0) {
        cy.submitWord(sampleWord)
        cy.registerScore(sampleScore, sampleWord) // assert - identical scoring
        cy.handShown()
        runTimes--
      }
      cy.displayWarning("You have submitted this word before using this current hand of letters. Try a different word or generate a new hand.")
    })
  })
})