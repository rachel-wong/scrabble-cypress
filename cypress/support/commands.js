Cypress.Commands.add("startNewGame", () => {
  cy.visit("/")
  cy.window((win) => {
    win.sessionStorage.clear()
  })
  cy.clearLocalStorage()
  cy.get('[data-cy="newGameBtn"]').should("exist").click()
})

Cypress.Commands.add("clearSessionStorage", () => {
  sessionStorage.clear()
})

Cypress.Commands.add("seedGameHistory", () => {
  let expectedHistory = [200, 300, 100, 50]
  localStorage.setItem('historyScores', JSON.stringify(expectedHistory))
})

Cypress.Commands.add("seedNewHand", (word) => {
  sessionStorage.setItem("hand", JSON.stringify([...word]))
})

Cypress.Commands.add("seedCurrentHistory", () => {
  localStorage.setItem("historyScores", JSON.stringify([]))
  localStorage.setItem("wordsFromHand", JSON.stringify([]))
})

Cypress.Commands.add("displayWarning", (text) => {
  cy.get('[data-cy="alertTextDisplay"]').should("be.visible")
    .and("have.text", text)
    .parent().and("have.class", "alert-warning")
})

Cypress.Commands.add("displayNotice", (text) => {
  cy.get('[data-cy="alertTextDisplay"]').should("be.visible")
    .and("have.text", text)
    .parent().and("have.class", "alert-info")
})

Cypress.Commands.add("submitWord", (word) => {
  cy.get('[data-cy="wordInputForm"]').type(word)
  cy.get('[data-cy="submitBtn"]').should("exist").click()
})

Cypress.Commands.add("showUI", (toggle) => {
  if (toggle) {
    cy.get('[data-cy="wordInputForm"]').should("be.visible")
    cy.get('[data-cy="wordSubmitted_Display"]').parent().should("be.visible")
    cy.get('[data-cy="handDisplay"]').parent().should("be.visible")
    cy.get('[data-cy="clearHistoryBtn"]').should("be.visible")
    cy.get('[data-cy="submitBtn"]').should("be.visible")
    cy.get('[data-cy="endGameBtn"]').should("be.visible")
    cy.get('[data-cy="gameScoreList"]').should("be.visible")
  } else {
    cy.get('[data-cy="wordInputForm"]').should("not.be.visible")
    cy.get('[data-cy="wordSubmitted_Display"]').parent().should("not.be.visible")
    cy.get('[data-cy="handDisplay"]').parent().should("not.be.visible")
    cy.get('[data-cy="clearHistoryBtn"]').should("not.be.visible")
    cy.get('[data-cy="submitBtn"]').should("not.be.visible")
    cy.get('[data-cy="endGameBtn"]').should("not.be.visible")
    cy.get('[data-cy="gameScoreList"]').should("not.be.visible")
  }
})

Cypress.Commands.add("nilHistory", () => {
  cy.get('[data-cy="gameScoreList"] li')
    .should("have.length", 1)
    .and("contain", "No history available")
})

Cypress.Commands.add("nilGameScore", () => {
  cy.get('[data-cy="currentScore"] li')
    .should("have.length", 0)
  cy.get('[data-cy="totalScore"]')
    .should("contain", 0)
})

Cypress.Commands.add("registerScore", (score, word) => {
  cy.get('[data-cy="scoreList"]').should("have.length", 1)
    .children().first()
    .and("contain", `${score} for ${word}`)
  cy.get('[data-cy="totalScore"]')
    .should("contain", score)
})

Cypress.Commands.add("seedStartScores", (currentScore, historyScores) => {
  sessionStorage.setItem("currentScore", JSON.stringify(currentScore))
  localStorage.setItem("historyScores", JSON.stringify(historyScores))
})

Cypress.Commands.add("noHandShown", () => {
  cy.get('[data-cy="handDisplay"]')
    .invoke('text')
    .then((hand) => {
      expect(hand).to.be.empty
    })
})

Cypress.Commands.add("wordNotAccepted", () => {
  cy.get('[data-cy="wordSubmitted_Display"]')
    .should((submission) => {
      expect(submission).to.be.empty
    })
})

Cypress.Commands.add("handShown", () => {
  cy.get('[data-cy="handDisplay"]')
    .should("be.visible")
    .and("contain", "")
})

Cypress.Commands.add("formClears", () => {
  cy.get('[data-cy="wordInputForm"]').should("exist")
    .and((submission) => {
      expect(submission).to.be.empty
    })
})