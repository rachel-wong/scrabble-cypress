/// <reference types="Cypress" />

describe("Generate New Hand", () => {
  beforeEach(() => {
    cy.startNewGame()
  })

  // Assert - starting from no hand, a new 7-char hand can be drawn
  it("when new game starts, 7-chars is drawn", () => {
    cy.get('[data-cy="handDisplay"]')
      .should("be.empty")
      .invoke('text').then((beforeHand) => {
        expect(beforeHand).to.be.empty
        cy.get('[data-cy="newHandBtn"]').should("exist").click()
        cy.get('[data-cy="handDisplay"]')
          .invoke('text').then((afterHand) => {
            afterHand = afterHand.split(",")
            expect(afterHand).to.have.lengthOf(7)
          })
      })
  })

  it("when there is a previous hand, a complete new 7-chars is drawn", () => {
    cy.seedCurrentHistory()
    cy.seedNewHand("inertia")
    const sampleWord = "tin"
    cy.submitWord(sampleWord)

    const remainingHand = "e,r,i,a"
    cy.get('[data-cy="handDisplay"]')
      .should("not.be.empty")
      .invoke("text")
      .then((firstHand) => {
        expect(firstHand).to.equal(remainingHand)
        firstHand = firstHand.split(",")
        expect(firstHand).to.have.lengthOf(4)
        cy.get('[data-cy="newHandBtn"]').should('exist').click()
        cy.get('[data-cy="handDisplay"]').should("not.be.empty")
          .invoke("text")
          .then((secondHand) => {
            expect(secondHand).to.not.equal(firstHand)
            secondHand = secondHand.split(",")
            expect(secondHand.length).to.be.greaterThan(firstHand.length)
            expect(secondHand).to.have.lengthOf(7)
          })
      })
  })
})