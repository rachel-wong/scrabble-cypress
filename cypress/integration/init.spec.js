/// <reference types="Cypress" />

describe("Base: Is Cypress working", () => {
  it("locally", () => {
    expect(true).to.equal(true)
  })
  it("on deployed netlify", () => {
    cy.startNewGame()
  })
})

describe("Initialise new game instance", () => {
  beforeEach(() => {
    cy.startNewGame()
  })

  it("Show welcome message", () => {
    cy.displayNotice("New game has started")
    cy.get('[data-cy="newGameBtn"]').should("not.be.visible")
  })

  it("Display UI", () => {
    cy.showUI(true)
    cy.get('[data-cy="newGameBtn"]').should("not.be.visible")
    cy.get('[data-cy="newLettersBtn"]').should("not.be.visible")
    cy.get('[data-cy="wordSubmitted_Display"]').should("be.empty")
    cy.get('[data-cy="handDisplay"]').should("be.empty")
  })

  it("A nil current game score exists", () => {
    cy.nilGameScore()
  })

  it("Show that no game history available", () => {
    cy.nilHistory()
  })
})