// Grab DOM elements
// const test = document.getElementById("test") // for testing things

// const wordSubmitted = document.getElementById('wordSubmitted')
// const hand = document.getElementById('hand')
// const currentScore = document.getElementById("currentScore")
// const totalCurrentScore = document.getElementById("totalScore")
// const scoreList = document.getElementById("scoreList")
// const gameScoreList = document.getElementById("gameScoreList")

// let hand = []
// let score = 0 // score for one submit
// let gameScores = [] // to collect all scores for a game

// Fetch Dictionary (no local server)
const getDictionary = async () => {
  try {
    let response = await fetch('words.txt')
    let data = await response.text() // returns a string
  } catch (err) {
    console.error(err)
  }
}
const globalData = await getDictionary()

const globalData = function goToGlobal(data) {
  return data
}

console.log('globalData:', globalData)

// Fetch Alphabetical score values (no local server)
async function getValues() {
  let requestHeaders = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
    mode: 'no-cors',
    cache: 'default'
  }
  let myRequest = new Request('./letter-values.json', requestHeaders)
  try {
    let response = await fetch(myRequest)
    let charValues = await response.json()
    return charValues
    // send charValues to controller method
  } catch (err) {
    console.error(err)
  }
}

// const globalValues = getValues()
// console.log('globalValues:', globalValues)

// CONTROLLER: Start new game clicked
function newGame() {
  let handLetters = getHand()
  hand.innerHTML = handLetters.join(", ")
}

// CONTROLLER: End game bttn clicked
function endGame() {}

// Get hand for user onload
// Can be triggered on button click
function getHand() {
  let hand = []
  let alphabets = "abcdefghijklmnopqrstuvwxyz"
  let len = 7
  for (let i = 0; i < len; i++) {
    hand.push(alphabets.charAt(Math.floor(Math.random() * alphabets.length)))
  }
  return hand
}

// Grab User Input from DOM
function submitWord() {
  // preventDefault to stop the form from automatically submitting
  // get current hand
  // trim whitespace form word & split into array
  // call calculate scores method
}

// Calculate Values
function getScore() {}