/// <reference types="Cypress" />

describe("More letters button", () => {
  beforeEach(() => {
    cy.startNewGame()
  })

  context("Can't generate more letters", () => {
    it("when hand is 0-char long", () => {
      cy.seedCurrentHistory()
      let sample = "inertia"
      cy.seedNewHand(sample)
      cy.submitWord(sample)
      cy.get('[data-cy="newLettersBtn"]').should("not.be.visible")
      cy.noHandShown()
    })

    it("when hand is 7-char long", () => {
      cy.seedCurrentHistory()
      cy.get('[data-cy="newHandBtn"]').should("be.visible").click()
      const emptyValue = " "
      cy.submitWord(emptyValue)
      cy.get('[data-cy="newLettersBtn"]').should("not.be.visible")
      cy.get('[data-cy="handDisplay"]')
        .invoke('text')
        .then((hand) => {
          expect(hand.split(",")).to.have.lengthOf(7)
        })
    })
  })

  context("Can generate more letters", () => {
    it("when hand is 4-char long, adds 3-char", () => {
      cy.seedCurrentHistory()
      let sampleHand = "inertia"
      cy.seedNewHand(sampleHand)
      const sampleWord = "tin"
      cy.submitWord(sampleWord)

      // Assert - can +3-chars to restore hand to 7-char
      cy.get('[data-cy="handDisplay"]').should("be.visible")
        .invoke('text')
        .then((firstHand) => {
          firstHand = firstHand.split(",")
          expect(firstHand).to.have.lengthOf(4)
          cy.get('[data-cy="newLettersBtn"]').should("exist").click()
          cy.get('[data-cy="handDisplay"]').should("be.visible")
            .invoke('text')
            .then((secondHand) => {
              expect(firstHand).to.not.eq(secondHand)
              secondHand = secondHand.split(",")
              expect(secondHand).to.have.lengthOf(7)
              expect(secondHand.length).to.be.greaterThan(firstHand.length)
            })
        })
    })
  })
})