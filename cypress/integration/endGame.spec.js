/// <reference types="Cypress" />

describe("End game instance", () => {
  beforeEach(() => {
    cy.visit("/")
  })

  it("Cannot end game without having a game started", () => {
    cy.get('[data-cy="newGameBtn"]').should("be.visible")
    cy.get('[data-cy="endGameBtn"]').should("not.be.visible")
  })

  it("Game already started, close UI", () => {
    cy.get('[data-cy="newGameBtn"]').should("be.visible").click()
    cy.showUI(true)
    cy.get('[data-cy="endGameBtn"]').should("be.visible").click()
    cy.showUI(false)
    cy.displayNotice("Game ended. Goodbye.")
    cy.get('[data-cy="newGameBtn"]').should("be.visible")
  })
})