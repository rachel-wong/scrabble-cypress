## SCRABBLE-ish

This is a challenge in learning Cypress and applying it to test user journeys through a very small vanillaJS app that follows some of the scenarios of a typical scrabble game. 

### Acceptance Criteria to aid and structure testing

> Assumes: Cypress web-driver accessing either the deployed or local liver server version of the app.

[Martin Fowler reference](https://martinfowler.com/bliki/GivenWhenThen.html)

[Dev notes](/devNotes.md)

:angry: *Using `data-cy` tag names for sanity's sake*

#### Create New Game instance

✅ Test passed  
**Given** User visits app for the first time  
**When** User clicks on New Game button  
**Then** Game UI displays on screeen  
**And** Notification displays "New game has started".  
**And** totalScore displays 0  
**And** and Game history displays "No game history available"  
**And** New Game button is not available

#### End Game instance

✅ Test passed  
**Given** A game is started  
**And** Game UI displays on screen  
**When** User clicks on End Game button  
**Then** Game UI is hidden from screen  
**And** End Game button is not available

✅ Test passed  
**Given** User visits app  
**When** User does not click on "New Game" button  
**And** Game UI is not displayed  
**Then** End game button is not available

#### Clear History

✅ Test passed  
**Given** User visits app for the first time  
**When** User clicks "New Game" button  
**And** User clicks on "Clear History" button  
**Then** Notification dispalys "All previous game scores erased."  
**And** Game history displays "No game history available"

✅ Test passed  
**Given** User visits app NOT for the first time  
**When** User clicks "New Game" button  
**And** User clicks on "Clear History" button  
**Then** Notification dipsplays "All previous game scores erased."  
**And** Game history displays "No game history available"

#### Form Input

✅ Test passed  
**Given** User is already on an active game  
**When** User submits a word in the form  
**Then** Notification displays "You must generate a hand first."  
**And** No change to currentScore and historyScores  
**And** Form clears

✅ Test passed  
**Given** User is already on an active game  
**When** User submits non-alphabetical characters in the form  
**Then** Notification displays "Alphabets only, no numbers or symbols."  
**And** No change to currentScore and historyScores  
**And** Form clears

✅ Test passed  
**Given** User is already on an active game  
**When** User submits nothing (empty spaces) in the form  
**Then** Notification displays "Alphabets only, no numbers or symbols."  
**And** No change to currentScore and historyScores  
**And** Form clears

##### Accumulate current score :boom: :boom: (halp)

✅ Test passed  
**Given** User is already on an active game  
**When** User clicks on "New Hand" button  
**And** User submits a word into the form  
**And** *The word is IN the dictionary*  
**And** The word is uses 6 or less characters from the hand  
**Then** WordSubmitted displays the word  
**And** scoreList adds an entry with "{num} for {word}"

✅ Test passed  
**Given** User is already on an active game  
**When** User clicks on "New Hand" button  
**And** User submits a word into the form  
**And** *The word is IN the dictionary*  
**And** The word is uses all 7 characters in the hand  
**Then** WordSubmitted displays the word  
**And** scoreList adds entry with "{num} for {word}"
**And** This produces a maximum score (note: not the optimal score)

✅ Test passed  
**Given** User is already on an active game  
**When** User clicks on "New Hand" button  
**And** User submits a word of any length in the form  
**And** *The word is NOT in the dictionary*  
**And** The word is only using the characters in the hand  
**Then** wordSubmitted does not populate  
**And** scoreList adds entry with "{num} for {word}"  

##### Accumulate total score

> Assume: User is on an active game and has an active hand

✅ Test passed  
**Given** User submits a word into input form  
**And** Word uses the hand characters  
**And** Word is in the dictionary  
**When** User clicks "submit" button  
**Then** currentScore increments (localStorage)  
**And** Totalscore increments with currentScore

##### Accumulate history score

✅ Test passed  
**Given** There is existing historyScores  
**When** User starts a new game  
**Then** Game history displays a running list of historyScores  

✅ Test passed  
**Given** There is no historyScores  
**When** User starts a new game  
**Then** Game history displays "No game history available"

✅ Test passed  
**Given** There is no historyScores  
**When** User starts a new game  
**And** Ends the game with 0 totalScore  
**And** Starts a game again  
**Then** Game history displays a single entry of '0'

✅ Test passed  
**Given** There is existing historyScores  
**When** User ends an existing game with a totalScore  
**And** User starts a game again  
**Then** Game history displays an additional entry with the totalScore

### Not quite AC's? :question:

*Could below ACs be closer to unit-level testing as oppose to being "business" scenario-oriented, value-driven requirements?*

*Can it be argued that they form technical litmus goalposts to support the hypothesis that app is functioning correctly?*

##### wordSubmitted display

**Given** No word has been submitted yet  
**When** User inputs a word in the form  
**And** User clicks on "Submit" button  
**Then** wordSubmitted displays word

**Given** A word is already submitted  
**When** User inputs a new word in the form  
**And** User clicks on "Submit" button"  
**Then** wordSubmitted displays the new word

##### Deal New Hand

**Given** User starts new game  
**And** handDisplay is empty  
**When** User clicks on "New Hand" button  
**Then** handDisplay diplays 7 characters separated by commas

**Given** User is already on an active game  
**And** handDisplay displays 7 characters separated by commas  
**When** User clicks on "New Hand" button  
**Then** handDisplay displays a new drawing of 7 characters, replacing the previous.

**Given** User is already on an active game  
**And** handDisplay displays less than 7 (or no) characters separated by commas  
**When** User clicks on "New Hand" button  
**Then** handDisplay displays a new drawing of 7 characters, replacing the previous.

##### More Letters button

✅ Test passed  
**Given** User visits app for the first time  
**When** User starts a new game  
**Then** "More Letters" button is not available

> Assumes: User is already on an active game

✅ Test passed  
**Given** User has not drawn a hand  
**When** User clicks on "New Hand"  
**And** handDisplay displays 7 characters  
**Then** "More letters" button is not available

✅ Test passed  
**Given** There is a hand  
**And** The hand has 7 characters  
**Then** "More letters" buttons is not available

✅ Test passed  
**Given** There is a hand  
**And** The hand has less than 6 characters long but more than 0  
**When** User clicks on "More Letters" button  
**Then** handDisplay the same hand with 1 additional letter

### Data validation

- Input form can accept any number of alphabetical characters

What is a scoring word?
1. Uses only alphabetical characters
2. Uses only the characters in the hand at any point in time
3. Does not include punctuation, numbers, symbols, spaces, nothing
4. Is in the dictionary
- No upper limit to scores
- a word that fulfils the above the criteria and is 7-letters in length achieves a maximum score

What is a hand? 
- A hand is 7 randomly drawn alphabetical characters
- A hand can have duplicating characters (randomize)