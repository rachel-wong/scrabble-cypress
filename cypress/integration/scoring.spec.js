/// <reference types="Cypress" />

describe("Increment Current Score", () => {
  beforeEach(() => {
    cy.startNewGame()
    cy.seedCurrentHistory()
    cy.seedNewHand("inertia")
  })

  context("Correct word: IN dictionary", () => {
    it("Score word shorter than 7-letters", () => {
      const sampleWord = "retain"
      const sampleScore = 36
      cy.submitWord(sampleWord)
      cy.registerScore(sampleScore, sampleWord)
    })

    // HIGHER SCORING
    it("Score 7-character word (max scoring)", () => {
      const sampleWord = "inertia"
      const sampleScore = 399

      cy.submitWord(sampleWord)
      cy.registerScore(sampleScore, sampleWord)
      cy.displayWarning("You have ran out of words!")
    })

    it("No score for word longer than 7-letters", () => {
      const sampleWord = "INERTIAE"
      cy.submitWord(sampleWord)
      cy.nilGameScore()
      cy.displayWarning("You must only use the number of letters in your hand.")
    })
  })

  context("Incorrect word: NOT in dictionary", () => {
    it("No score for word shorter than 7-letters", () => {
      const sampleWord = "nert"
      cy.submitWord(sampleWord)
      cy.nilGameScore()
      cy.displayWarning("Word is not found in dictionary. Try again")
    })

    it("No score for word longer than 7-letters", () => {
      const sampleWord = "nerttttt"
      cy.submitWord(sampleWord)
      cy.nilGameScore()
      cy.displayWarning("You must only use the number of letters in your hand.")
    })
  })
})