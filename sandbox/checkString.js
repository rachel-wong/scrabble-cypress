// function scramble(string1, string2) {
//   return string2.split('').every(function (character) {
//     // Every string2 characters must be included in the string1
//     return string1.includes(character)
//   })
// }

// // false
// console.log(scramble('katas', 'stakaa'));

// for every item in submitted
// there is a match in hand
// remove the match in hand
// move to next item in submitted
// if gets to end of submitted
// return true
// else
// return false


// REVERSE
// for every item in hand
// if there is a match in submitted
// remove match in submitted
// move to next item in hand
// if submitted === [] return true
// else return false

function failedCheckString(hand, submitted) {
  submitted = submitted.split("")
  hand = hand.split("")
  for (let i = 0; i < hand.length; i++) {
    let removeIndex = submitted.indexOf(hand[i])
    if (i < hand.length && submitted.length === 0) {
      return true
    } else if (removeIndex != -1) {
      submitted.splice(removeIndex, 1)
    } else {
      return false
    }
  }
}

// submitted has all of hand => true
// submitted does not have all of hand => false
// find difference between the two 
function anotherfailedcheckString(hand, submitted) {
  submitted = submitted.split("")
  hand = hand.split("")
  let difference = arr1
    .filter(x => !arr2.includes(x))
    .concat(arr2.filter(x => !arr1.includes(x)));
  console.log('difference:', difference)
}

function thirdfailedcheckString(hand, submitted) {
  hand = hand.split("").sort().join("")
  console.log('hand:', hand)
  submitted = submitted.split("").sort().join("")
  console.log('submitted:', submitted)

  if (hand.localeCompare(submitted) === 1) {
    return false
  } else {
    return true
  }
}

function noworkcheckString(hand, submitted) {
  hand = hand.split("").sort()
  submitted = submitted.split("").sort()

  if (hand.includes(submitted) == true) {
    return true
  } else {
    return false
  }
  // return submitted.includes(hand) ? true : false
}

function nocheckString(hand, submitted) {
  // submitted needs to be in all of hand
  let isHere = 0
  hand = hand.split("").sort()
  submitted = submitted.split("").sort()
  for (let i = 0; i < submitted.length; i++) {
    if (hand.indexOf(submitted[i]) == true) {
      console.log(hand.indexOf(submitted[i]))
      return true
    } else {
      return false
    }
  }
  // if (isHere === hand.length && isHere) {
  //   return true
  // } else {
  //   return false
  // }
}

function checkString(hand, submitted) {
  let count = {}
  hand.split("").forEach(char => {
    count[char] = (count[char] || 0) + 1
  })
  submitted = submitted.split("")
  for (let i = 0; i < submitted.length; i++) {
    if (count[submitted[i]]) {
      count[submitted[i]] -= 1
    } else if (count[submitted[i]] === undefined) {
      return false
    } else {
      count[submitted[i]] -= 1
    }
  }
  let anyNegatives = Object.values(count).filter(val => {
    return val < 0
  })
  if (anyNegatives.length != 0) {
    return false
  } else {
    return true
  }

}

console.log(checkString("trarcrh", "rach"))
// console.log(checkString('katas', 'sqakaa')) // false
// console.log(checkString('katas', 'qqqqq')) // false
// console.log(checkString('katas', 'stakaa')) // false
// console.log(checkString('katas', 'katssss')) // false
// console.log(checkString('ineraia', 'inert')) // false
// console.log(checkString('katas', 'staka')) // true
// console.log(checkString('katas', 'sta')) // true
// console.log(checkString('katas', 'a')) // true
// console.log(checkString('katas', 't')) // true