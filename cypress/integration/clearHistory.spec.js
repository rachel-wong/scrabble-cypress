/// <reference types="Cypress" />

describe("Clear History Button", () => {
  beforeEach(() => {
    cy.clearSessionStorage()
  })

  it("Start brand-new game with no history", () => {
    cy.startNewGame()
    cy.get('[data-cy="clearHistoryBtn"]').should("exist").click()
    cy.displayNotice("All previous game scores erased.")
    cy.nilHistory()
    cy.showUI(true)
  })

  it("Start game session with history scores", () => {
    cy.seedGameHistory()
    cy.visit("/")
    cy.get('[data-cy="newGameBtn"]').should("be.visible").click()
    cy.get('[data-cy="gameScoreList"] li').should("have.length", 4)
    cy.showUI(true)
    cy.get('[data-cy="clearHistoryBtn"]').should("be.visible").click()
    cy.get('[data-cy="newGameBtn"]').should("not.be.visible")
    cy.displayNotice("All previous game scores erased.")
    cy.nilHistory()
  })
})