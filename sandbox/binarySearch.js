// function binarySearch(array, target) {
//   let startIndex = 0
//   let endIndex = array.length - 1
//   while (startIndex <= endIndex) {
//     let middleIndex = Math.floor((startIndex + endIndex) / 2)
//     if (target === array[middleIndex]) {
//       return `Target was found at index ${middleIndex}`
//     }
//     if (target > array[middleIndex]) {
//       console.log("Searching the right side of Array")
//       startIndex = middleIndex + 1
//     }
//     if (target < array[middleIndex]) {
//       console.log("Searching the left side of array")
//       endIndex = middleIndex - 1
//     } else {
//       return `Not Found this loop iteration. Looping another iteration.`
//     }
//   }
//   return `Target value not found in array`
// }

let dictionary = ["apple", "banana", "eggplant", "grapefruit", "orange", "pineapple", "tomato", "zucchini"]

// Does not truncate the array and is not recursive, hopefully more performant?
function iterativeFunction(arr, target) {
  // takes advantage of the fact that js measures alphabets in string
  let startIndex = 0
  let endIndex = arr.length - 1
  // Iterate while start not meets end 
  while (startIndex <= endIndex) {
    // Find the mid index 
    let midIndex = Math.floor((startIndex + endIndex) / 2)
    // If element is right in the middle of the array, then yes it is found 
    if (arr[midIndex] === target) {
      return true
    }
    // move to the right side of the current middle of the array 
    else if (arr[midIndex] < target) {
      startIndex = midIndex + 1
    }
    // move to the left side of the current middle of the array
    else {
      endIndex = midIndex - 1
    }
  }
  return false // completely not in the array when start meets the end
}

console.log(iterativeFunction(dictionary, "bees"))