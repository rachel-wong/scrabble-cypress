describe("Game history scores", () => {
  context("Display history", () => {
    beforeEach(() => {
      cy.clearLocalStorage()
      cy.clearSessionStorage()
    })

    it("Show no history for brand new player", () => {
      cy.startNewGame()
      cy.nilHistory()
    })

    it("Show history for returning player", () => {
      cy.seedGameHistory()
      cy.visit("/")
      cy.get('[data-cy="newGameBtn"]').should("exist").click()
      cy.get('[data-cy="gameScoreList"] li').should("have.length", 4)
      cy.get('[data-cy="gameScoreList"]').children().first().should("contain", 200)
    })
  })

  context("Accumulate history", () => {
    beforeEach(() => {
      cy.startNewGame()
    })

    it("adds score of 15 to historyScores", () => {
      let currentScore = 15
      let historyScores = []

      cy.seedStartScores(currentScore, historyScores)
      cy.nilHistory()
      cy.get('[data-cy="endGameBtn"]').should("exist").click()
      cy.get('[data-cy="newGameBtn"]').should("exist").click()
      cy.nilGameScore()
      cy.get('[data-cy="gameScoreList"]')
        .should("have.length", 1)
        .first()
        .and("contain", currentScore)
    })

    it("adds score of 0 to historyScores", () => {
      let currentScore = 0
      let historyScores = []

      cy.seedStartScores(currentScore, historyScores)
      cy.nilGameScore()
      cy.nilHistory()
      cy.get('[data-cy="endGameBtn"]').should("exist").click()
      cy.get('[data-cy="newGameBtn"]').should("exist").click()
      cy.nilGameScore()
      cy.get('[data-cy="gameScoreList"]')
        .should("have.length", 1)
        .children().first()
        .and("contain", currentScore)
    })
  })
})